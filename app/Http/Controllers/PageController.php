<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    function about() {
        $tags = ['tag1', 'tag2', 'tag3'];
        return view('about', ['tags' => $tags]);
    }
}
